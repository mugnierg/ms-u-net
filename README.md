# Test of the performance of MindSpore
This project aims to learn MindSpore and evaluate its performance. Our usecase is semantic segmentation on lungs images. We use U-Net 3D from the MindSpore's model zoo.

## Requirements
Use your favorite library manager (for example pipenv) to install the requirements :
```bash
cd unet3d_1.10
pipenv shell
pipenv install requirements.txt
```

## Some help
You can find some instructions to begin with the model in the Instruction.md document.

## Results
The table Unet3D.xlsx resume the results so far on the LUNA dataset with 1, 2, 4 and 8 accelerators using Auto-Par.
